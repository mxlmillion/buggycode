FROM debian AS java-deadlock-builder
RUN apt-get update
RUN apt-get install -y openjdk-8-jdk-headless
RUN apt-get install -y procps
RUN apt-get install -y man-db

FROM java-deadlock-builder 
WORKDIR	/app/bugprogs
COPY	. .
RUN javac DeadlockProgram.java
RUN javac MatchLoop.java
#ENTRYPOINT ["java"]
#CMD ["DeadlockProgram"]
